package fr.tarkod.defaultmessage;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import fr.tarkod.defaultmessage.listeners.PlayerJoin;
import fr.tarkod.defaultmessage.listeners.PlayerMessage;
import fr.tarkod.defaultmessage.listeners.PlayerQuit;

public class DefaultMessageMain extends JavaPlugin {
	
	static DefaultMessageMain instance;
	static FileConfiguration config;
	static File cfile;
	
	public static DefaultMessageMain getInstance() {
		return instance;
	}
	
	@Override
	public void onEnable() {
		instance = this;
		config = getConfig();
		config.options().copyDefaults(true);
		saveConfig();
		cfile = new File(getDataFolder() + "config.yml");
		
		getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
		getServer().getPluginManager().registerEvents(new PlayerQuit(), this);
		getServer().getPluginManager().registerEvents(new PlayerMessage(), this);
	}

}
