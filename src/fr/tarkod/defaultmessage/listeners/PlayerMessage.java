package fr.tarkod.defaultmessage.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import fr.tarkod.defaultmessage.DefaultMessageMain;

public class PlayerMessage implements Listener {
	
	public DefaultMessageMain main = DefaultMessageMain.getInstance();
	
	@EventHandler
	public void onMessage(AsyncPlayerChatEvent e) {
		e.setCancelled(true);
		Bukkit.broadcastMessage(main.getConfig().getString("message").replace("%player%", e.getPlayer().getName()).replace("%message%", e.getMessage()));
	}
}
