package fr.tarkod.defaultmessage.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import fr.tarkod.defaultmessage.DefaultMessageMain;

public class PlayerJoin implements Listener {
	
	public DefaultMessageMain main = DefaultMessageMain.getInstance();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if(p.hasPlayedBefore()) {
			e.setJoinMessage(main.getConfig().getString("join_message").replace("%player%", e.getPlayer().getName()));
			return;
		} else {
			e.setJoinMessage(main.getConfig().getString("first_join_message").replace("%player%", e.getPlayer().getName()));
			return;
		}
	}
}
