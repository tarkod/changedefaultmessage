package fr.tarkod.defaultmessage.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.tarkod.defaultmessage.DefaultMessageMain;

public class PlayerQuit implements Listener {
	
	public DefaultMessageMain main = DefaultMessageMain.getInstance();
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		e.setQuitMessage(main.getConfig().getString("quit_message").replace("%player%", e.getPlayer().getName()));
	}
}
